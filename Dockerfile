#De la imagen que partimos
FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.6_10
VOLUME /tmp
EXPOSE 9002
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.ws.bcp-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/app.ws.bcp-0.0.1-SNAPSHOT.jar"]
