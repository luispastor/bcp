package app.ws.bcp.exception;


import lombok.Data;

@Data
public class ValidateException extends RuntimeException{
    private final String code;
    private final String message;

    public ValidateException(String code, String message) {
        super(message);
        this.code=code;
        this.message=message;
    }



}
