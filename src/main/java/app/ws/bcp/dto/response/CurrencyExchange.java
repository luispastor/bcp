package app.ws.bcp.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrencyExchange {

    private BigDecimal amountOrigin;
    private BigDecimal amountDestiny;
    private BigDecimal exchangeRate;
    private String codeCurrencyOrigin;
    private String codeCurrencyDestiny;

}
