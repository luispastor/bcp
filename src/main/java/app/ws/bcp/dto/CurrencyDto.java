package app.ws.bcp.dto;

import app.ws.bcp.model.Currency;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrencyDto {

    private Long id;

    @NotEmpty(message = "campo es requerido")
    private String code;

    private String name;

    private List<ExchangeRateDto> exchangeRates = new ArrayList<>();

    public static CurrencyDto toDto(Currency model) {
        if (model == null) return null;

        return CurrencyDto.builder()
                .id(model.getId())
                .code(model.getCode())
                .name(model.getName())
                .exchangeRates(ExchangeRateDto.toDto(model.getExchangeRates()))
                .build();
    }

    public static List<CurrencyDto> toDto(List<Currency> models) {
        if (models == null) return Collections.emptyList();
        return models.stream()
                .map(CurrencyDto::toDto).collect(Collectors.toList());
    }
}
