package app.ws.bcp.dto.request;

import app.ws.bcp.dto.CurrencyDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrencyExchangeRequest {

    @Positive(message = "campo debe ser valor positivo")
    private BigDecimal amountOrigin;

    @NotNull(message = "campo no debe ser nulo")
    private CurrencyDto currencyOrigin;

    @NotNull(message = "campo no debe ser nulo")
    private CurrencyDto currencyDestiny;
}
