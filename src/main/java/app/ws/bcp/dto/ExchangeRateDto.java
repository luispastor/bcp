package app.ws.bcp.dto;

import app.ws.bcp.model.ExchangeRate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.reactivex.Single;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExchangeRateDto {

    private Long id;

    @Positive(message = "campo debe ser valor positivo")
    private BigDecimal amount;

    private String codeCurrency;

    @NotNull(message = "campo no debe ser nulo")
    public CurrencyDto currency;


    public static ExchangeRateDto toDto(ExchangeRate model) {
        if (model == null) return null;

        return ExchangeRateDto.builder()
                .id(model.getId())
                .amount(model.getAmount())
                .codeCurrency(model.getCodeCurrency())
                //.currency(CompanyDto.toDto(model.getCompany()))
                .build();
    }

    public static List<ExchangeRateDto> toDto(List<ExchangeRate> models) {
        if (models == null) return Collections.emptyList();
        return models.stream()
                .map(ExchangeRateDto::toDto).collect(Collectors.toList());
    }
}
