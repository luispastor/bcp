package app.ws.bcp.repository;

import app.ws.bcp.model.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long> {

    @Query(value = "select e from ExchangeRate e" +
            " where e.currency.id =:idCurrency and " +
            " e.codeCurrency =:codeCurrency or " +
            " ( :idCurrencyDestiny is null or e.currency.id =:idCurrencyDestiny )")
    Optional<ExchangeRate> findByCurrency(@Param("idCurrency") Long idCurrency, @Param("codeCurrency") String codeCurrency,
                                          @Param("idCurrencyDestiny") Long idCurrencyDestiny);


    @Query(value = "select e from ExchangeRate e" +
            " where e.currency.id =:idCurrency and " +
            " e.codeCurrency =:codeCurrency ")
    Optional<ExchangeRate> findByCurrency(@Param("idCurrency") Long idCurrency, @Param("codeCurrency") String codeCurrency);
}

