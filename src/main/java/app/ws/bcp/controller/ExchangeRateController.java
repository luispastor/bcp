package app.ws.bcp.controller;

import app.ws.bcp.dto.ExchangeRateDto;
import app.ws.bcp.dto.request.CurrencyExchangeRequest;
import app.ws.bcp.dto.response.CurrencyExchange;
import app.ws.bcp.model.ExchangeRate;
import app.ws.bcp.service.CurrencyExchangeService;
import app.ws.bcp.service.ExchangeRateService;
import app.ws.bcp.util.UtilField;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1/exhange-rate")
public class ExchangeRateController {

    private final ExchangeRateService exchangeRateService;
    private final CurrencyExchangeService currencyExchangeService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<ExchangeRate>> save(@Valid @RequestBody ExchangeRateDto dto, BindingResult result) {

        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, UtilField.formatMessage(result));
        }


        return exchangeRateService.save(ExchangeRate.toModel(dto)).subscribeOn(Schedulers.io()).map(
                s -> ResponseEntity.status(HttpStatus.CREATED)
                        .body(s));
    }

    @PostMapping(value = "/calculate", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<CurrencyExchange>> currencyChange(@Valid @RequestBody CurrencyExchangeRequest request, BindingResult result) {

        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, UtilField.formatMessage(result));
        }
        return currencyExchangeService.calcule(request).subscribeOn(Schedulers.io()).map(
                s -> ResponseEntity.status(HttpStatus.OK)
                        .body(s));
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<CurrencyExchange>> update(@Valid @RequestBody ExchangeRateDto dto, BindingResult result) {

        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, UtilField.formatMessage(result));
        }

        return exchangeRateService.update(ExchangeRate.toModel(dto))
                .subscribeOn(Schedulers.io())
                .toSingle(() -> ResponseEntity.ok(CurrencyExchange.builder().build()));

    }

}
