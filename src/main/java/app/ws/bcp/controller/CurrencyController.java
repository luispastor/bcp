package app.ws.bcp.controller;

import app.ws.bcp.dto.CurrencyDto;
import app.ws.bcp.dto.ExchangeRateDto;
import app.ws.bcp.model.Currency;
import app.ws.bcp.model.ExchangeRate;
import app.ws.bcp.service.CurrencyService;
import app.ws.bcp.util.UtilField;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1/currency")
public class CurrencyController {

    private final CurrencyService currencyService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CurrencyDto> save(@Valid @RequestBody CurrencyDto dto, BindingResult result) {

        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, UtilField.formatMessage(result));
        }
        var currency = currencyService.save(Currency.toModel(dto));

        return ResponseEntity.status(HttpStatus.CREATED).body(CurrencyDto.toDto(currency));
    }

    @GetMapping
    public ResponseEntity<List<CurrencyDto>> findAll() {

        var list = currencyService.findAll();
        if (list.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(CurrencyDto.toDto(list));
        }else {
            return ResponseEntity.status(HttpStatus.OK).body(CurrencyDto.toDto(list));
        }

    }
}
