package app.ws.bcp.model;

import app.ws.bcp.dto.CurrencyDto;
import app.ws.bcp.dto.ExchangeRateDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "exchange_rate")
@Builder
public class ExchangeRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "code_currency")
    private String codeCurrency;

    @ManyToOne
    @JoinColumn(name = "id_currency")
    public Currency  currency;

    public static ExchangeRate toModel(ExchangeRateDto dto) {
        if (dto == null) return null;

        return ExchangeRate.builder()
                .id(dto.getId())
                .amount(dto.getAmount())
                .codeCurrency(dto.getCodeCurrency())
                .currency(Currency.toModel(dto.getCurrency()))
                .build();
    }

    public static List<ExchangeRate> toModel(List<ExchangeRateDto> dtos) {
        if (dtos == null) return Collections.emptyList();
        return dtos.stream()
                .map(ExchangeRate::toModel).collect(Collectors.toList());
    }
}
