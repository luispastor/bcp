package app.ws.bcp.model;

import app.ws.bcp.dto.CurrencyDto;
import app.ws.bcp.dto.ExchangeRateDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "currency")
@Builder
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "currency", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ExchangeRate> exchangeRates = new ArrayList<>();


    public static Currency toModel(CurrencyDto dto) {
        if (dto == null) return null;

        return Currency.builder()
                .id(dto.getId())
                .code(dto.getCode())
                .name(dto.getName())
                .exchangeRates(ExchangeRate.toModel(dto.getExchangeRates()))
                .build();
    }

    public static List<Currency> toModel(List<CurrencyDto> dtos) {
        if (dtos == null) return Collections.emptyList();
        return dtos.stream()
                .map(Currency::toModel).collect(Collectors.toList());
    }

}
