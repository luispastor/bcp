package app.ws.bcp.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiError {

    private HttpStatus status;
    private String message;

    ApiError(HttpStatus status) {
        this.status = status;
    }
    ApiError(HttpStatus status, String message, Throwable ex) {

        this.status = status;
        this.message = message;

    }


}

