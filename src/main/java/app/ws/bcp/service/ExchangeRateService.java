package app.ws.bcp.service;

import app.ws.bcp.model.ExchangeRate;
import io.reactivex.Completable;
import io.reactivex.Single;

public interface ExchangeRateService {

    Single<ExchangeRate> save(ExchangeRate model);
    Completable update(ExchangeRate model);

}
