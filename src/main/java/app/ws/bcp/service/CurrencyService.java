package app.ws.bcp.service;

import app.ws.bcp.model.Currency;

import java.util.List;

public interface CurrencyService {

    Currency save(Currency model);
    List<Currency> findAll();
}
