package app.ws.bcp.service;

import app.ws.bcp.dto.request.CurrencyExchangeRequest;
import app.ws.bcp.dto.response.CurrencyExchange;
import io.reactivex.Single;

public interface CurrencyExchangeService {

    Single<CurrencyExchange> calcule(CurrencyExchangeRequest request);
}
