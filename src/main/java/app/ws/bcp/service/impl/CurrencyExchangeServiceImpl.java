package app.ws.bcp.service.impl;

import app.ws.bcp.dto.request.CurrencyExchangeRequest;
import app.ws.bcp.dto.response.CurrencyExchange;
import app.ws.bcp.exception.ValidateException;
import app.ws.bcp.model.ExchangeRate;
import app.ws.bcp.repository.ExchangeRateRepository;
import app.ws.bcp.service.CurrencyExchangeService;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {
    private final ExchangeRateRepository exchangeRateRepository;

    @Override
    public Single<CurrencyExchange> calcule(CurrencyExchangeRequest request) {

        return Single.create(singleSubscriber -> {
            Optional<ExchangeRate> modelExist = exchangeRateRepository.findByCurrency(request.getCurrencyOrigin().getId(),
                    request.getCurrencyDestiny().getCode(), request.getCurrencyDestiny().getId());
            if (!modelExist.isPresent())
                singleSubscriber.onError(new ValidateException("500", String.format("No existe tipo de cambio de %s para %s",
                        request.getCurrencyOrigin().getCode(), request.getCurrencyDestiny().getCode())));
            else {
                    var amount = BigDecimal.ZERO;
                if (request.getCurrencyOrigin().getCode().equals(modelExist.get().getCurrency().getCode())) {
                    amount = request.getAmountOrigin().multiply(modelExist.get().getAmount());
                } else {
                    amount = request.getAmountOrigin().divide(modelExist.get().getAmount(), 2, RoundingMode.HALF_UP);

                }
               var currency = CurrencyExchange.builder()
                        .codeCurrencyOrigin(request.getCurrencyOrigin().getCode())
                        .codeCurrencyDestiny(request.getCurrencyDestiny().getCode())
                        .amountOrigin(request.getAmountOrigin())
                        .amountDestiny(amount)
                        .exchangeRate(modelExist.get().getAmount())
                        .build();
                singleSubscriber.onSuccess(currency);
            }
        });
    }
}
