package app.ws.bcp.service.impl;

import app.ws.bcp.exception.ValidateException;
import app.ws.bcp.model.ExchangeRate;
import app.ws.bcp.repository.ExchangeRateRepository;
import app.ws.bcp.service.ExchangeRateService;
import io.reactivex.Completable;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
@Slf4j
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private final ExchangeRateRepository exchangeRateRepository;


    @Override
    public Single<ExchangeRate> save(ExchangeRate model) {
        return saveExchangeRate(model);
    }

    private Single<ExchangeRate> saveExchangeRate(ExchangeRate model) {

        return Single.create(singleSubscriber -> {
            Optional<ExchangeRate> modelExist = exchangeRateRepository.findByCurrency(model.getCurrency().getId(),
                    model.getCodeCurrency());
            if (modelExist.isPresent())
                singleSubscriber.onError(new ValidateException("500", "La moneda ya tiene tipo de cambio "));
            else {
                var exchangeRate = exchangeRateRepository.save(model);
                singleSubscriber.onSuccess(exchangeRate);
            }
        });
    }

    @Override
    public Completable update(ExchangeRate model) {
        return Completable.create(completableSubscriber -> {
            Optional<ExchangeRate> modelExist = exchangeRateRepository.findByCurrency(model.getCurrency().getId(),
                    model.getCodeCurrency());
            if (!modelExist.isPresent())
                completableSubscriber.onError(new EntityNotFoundException());
            else {
                var exchange = modelExist.get();
                exchange.setAmount(model.getAmount());
                exchangeRateRepository.save(exchange);
                completableSubscriber.onComplete();
            }
        });

    }
}
