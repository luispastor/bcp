package app.ws.bcp.service.impl;

import app.ws.bcp.model.Currency;
import app.ws.bcp.repository.CurrencyRepository;
import app.ws.bcp.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    @Override
    public Currency save(Currency model) {
        return currencyRepository.save(model);
    }

    @Override
    public List<Currency> findAll() {
        return currencyRepository.findAll();
    }
}
